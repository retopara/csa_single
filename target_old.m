function value = target(A1,A2,B1,B2,va1,va2,vb1,vb2,thetaa1,thetaa2,thetab1,thetab2,thetao1,thetao2,thetao3,thetao4,theta1,theta2,phi1,phi2)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

m=-1;
n=-1;
pi = 3.1415926;
i=sqrt(-1);
VA1p=[cos(A1*pi),exp(i*thetaa1*pi)*sin(A1*pi)];
VA1m=[-exp(-i*thetaa1*pi)*sin(A1*pi), cos(A1*pi)];
VA2p=[cos(A2*pi), exp(i*thetaa2*pi)*sin(A2*pi)];
VA2m=[-exp(-i*thetaa2*pi)*sin(A2*pi), cos(A2*pi)];
VB1p=[cos(B1*pi), exp(i*thetab1*pi)*sin(B1*pi)];
VB1m=[-exp(-i*thetab1*pi)*sin(B1*pi), cos(B1*pi)];
VB2p=[cos(B2*pi), exp(i*thetab2*pi)*sin(B2*pi)];
VB2m=[-exp(-i*thetab2*pi)*sin(B2*pi), cos(B2*pi)];

OrientA1=[cos(va1*pi),exp(i*thetao1*pi)*sin(va1*pi)];
OrientA2=[cos(va2*pi),exp(i*thetao2*pi)*sin(va2*pi)];
OrientB1=[cos(vb1*pi),exp(i*thetao3*pi)*sin(vb1*pi)];
OrientB2=[cos(vb2*pi),exp(i*thetao4*pi)*sin(vb2*pi)];

ro=[1/sqrt(2),0,0,1/sqrt(2)];
U1=[cos(theta1*pi),exp(i*phi1)*sin(theta1*pi);-exp(-i*phi1)*sin(theta1*pi),cos(theta1*pi)];
U2=[cos(theta2*pi),exp(i*phi2)*sin(theta2*pi);-exp(-i*phi2)*sin(theta2*pi),cos(theta2*pi)];
U=kron(U1,U2);
ro =U*ro';


NVA1p = cos(va1*pi)*VA1p +  exp(i*thetao1*pi)*sin(va1*pi)*VA2p;
NVA1m = cos(va1*pi)*VA1m +  exp(i*thetao1*pi)*sin(va1*pi)*VA2m;
NVA2p = cos(va2*pi)*VA1p +  exp(i*thetao2*pi)*sin(va2*pi)*VA2p;
NVA2m = cos(va2*pi)*VA1m +  exp(i*thetao2*pi)*sin(va2*pi)*VA2m;

NVA1p = conj(NVA1p)/sqrt(NVA1p*NVA1p');
NVA1m = conj(NVA1m)/sqrt(NVA1m*NVA1m');
NVA2p = conj(NVA2p)/sqrt(NVA2p*NVA2p');
NVA2m = conj(NVA2m)/sqrt(NVA2m*NVA2m');

NVB1p = cos(vb1*pi)*VB1p + exp(i*thetao3*pi)*sin(vb1*pi)*VB2p;
NVB1m = cos(vb1*pi)*VB1m + exp(i*thetao3*pi)*sin(vb1*pi)*VB2m;
NVB2p = cos(vb2*pi)*VB1p + exp(i*thetao4*pi)*sin(vb2*pi)*VB2p;
NVB2m = cos(vb2*pi)*VB1m + exp(i*thetao4*pi)*sin(vb2*pi)*VB2m;

NVB1p=conj(NVB1p)/sqrt(NVB1p*NVB1p');
NVB1m=conj(NVB1m)/sqrt(NVB1m*NVB1m');
NVB2p=conj(NVB2p)/sqrt(NVB2p*NVB2p');
NVB2m=conj(NVB2m)/sqrt(NVB2m*NVB2m');

Pa1b1pp=(abs(kron(NVA1p,NVB1p)*ro))^2;
Pa1b1mm=(abs(kron(NVA1m,NVB1m)*ro))^2;
Pa1b1pm=(abs(kron(NVA1p,NVB1m)*ro))^2;
Pa1b1mp=(abs(kron(NVA1m,NVB1p)*ro))^2;

Pa1b2pp=(abs(kron(NVA1p,NVB2p)*ro))^2;
Pa1b2mm=(abs(kron(NVA1m,NVB2m)*ro))^2;
Pa1b2mp=(abs(kron(NVA1m,NVB2p)*ro))^2;
Pa1b2pm=(abs(kron(NVA1p,NVB2m)*ro))^2;

Pa2b1pp=(abs(kron(NVA2p,NVB1p)*ro))^2;
Pa2b1mm=(abs(kron(NVA2m,NVB1m)*ro))^2;
Pa2b1mp=(abs(kron(NVA2m,NVB1p)*ro))^2;
Pa2b1pm=(abs(kron(NVA2p,NVB1m)*ro))^2;

Pa2b2pp=(abs(kron(NVA2p,NVB2p)*ro))^2;
Pa2b2mm=(abs(kron(NVA2m,NVB2m)*ro))^2;
Pa2b2mp=(abs(kron(NVA2m,NVB2p)*ro))^2;
Pa2b2pm=(abs(kron(NVA2p,NVB2m)*ro))^2;


%{
[NPa1b1pp NPa1b1pm NPa1b1mp NPa1b1mm NPa2b1pp NPa2b1pm NPa2b1mp NPa2b1mm ...,
    NPa1b2pp NPa1b2pm NPa1b2mp NPa1b2mm NPa2b2pp NPa2b2pm NPa2b2mp NPa2b2mm]=solve(NPa1b1pp+NPa1b1pm-NPa1b2pp-NPa1b2pm,NPa1b1mp+NPa1b1mm-NPa1b2mp-NPa1b2mm, ...,
    NPa2b1pp+NPa2b1pm-NPa2b2pp-NPa2b2pm,NPa2b1mp+NPa2b1mm-NPa2b2mp-NPa2b2mm, ...,
    NPa1b1pp+NPa1b1mp-NPa2b1pp-NPa2b1mp,NPa1b1pm+NPa1b1mm-NPa2b1pm-NPa2b1mm, ...,
    NPa1b2pp+NPa1b2mp-NPa2b2pp-NPa2b2mp,NPa1b2pm+NPa1b2mm-NPa2b2pm-NPa2b2mm, ...,
    Pa1b1pm*NPa1b1pp-Pa1b1pp*NPa1b1pm,Pa1b1mm*NPa1b1mp-Pa1b1mp*NPa1b1mm, ...,
    Pa2b1pm*NPa2b1pp-Pa2b1pp*NPa2b1pm,Pa2b1mm*NPa2b1mp-Pa2b1mp*NPa2b1mm, ...,
    Pa1b2pp*NPa1b2mp-Pa1b2mp*NPa1b2pp,Pa1b2pm*NPa1b2mm-Pa1b2mm*NPa1b2pm, ...,
    Pa2b2pp*NPa2b2mp-Pa2b2mp*NPa2b2pp,Pa2b2pm*NPa2b2mm-Pa2b2mm*NPa2b2pm, ...,
    NPa1b1pp+NPa1b1mp+NPa1b1pm+NPa1b1mm-1,NPa1b2pp+NPa1b2mp+NPa1b2pm+NPa1b2mm-1, ...,
    NPa2b1pp+NPa2b1mp+NPa2b1pm+NPa2b1mm-1,NPa2b2pp+NPa2b2mp+NPa2b2pm+NPa2b2mm-1)

                                                                           %order: a1b1pp a1b1pm a1b1mp a1b1mm a2b1pp a2b1pm a2b1mp a2b1mm
                                                                           %       a1b2pp a1b2pm a1b2mp a1b2mm a2b2pp a2b2pm a2b2mp a2b2mm

MA=[1,1,0,0,0,0,0,0,-1,-1,0,0,0,0,0,0;
   0,0,1,1,0,0,0,0,0,0,-1,-1,0,0,0,0;
   0,0,0,0,1,1,0,0,0,0,0,0,-1,-1,0,0;
   0,0,0,0,0,0,1,1,0,0,0,0,0,0,-1,-1;
   1,0,1,0,-1,0,-1,0,0,0,0,0,0,0,0,0;
   0,1,0,1,0,-1,0,-1,0,0,0,0,0,0,0,0;
   0,0,0,0,0,0,0,0,1,0,1,0,-1,0,-1,0;
   0,0,0,0,0,0,0,0,0,1,0,1,0,-1,0,-1;
   Pa1b1pm,-Pa1b1pm,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
   0,0,Pa1b1mm,-Pa1b1mp,0,0,0,0,0,0,0,0,0,0,0,0;
   0,0,0,0,Pa2b1pm,-Pa2b1pp,0,0,0,0,0,0,0,0,0,0;
   0,0,0,0,0,0,Pa2b1mm,-Pa2b1mp,0,0,0,0,0,0,0,0;
   0,0,0,0,0,0,0,0,Pa1b2pp,0,-Pa1b2mp,0,0,0,0,0;
   0,0,0,0,0,0,0,0,0,Pa1b2pm,0,-Pa1b2mm,0,0,0,0;
   0,0,0,0,0,0,0,0,0,0,0,0,Pa2b2pp,0,-Pa2b2mp,0;
   0,0,0,0,0,0,0,0,0,0,0,0,0,Pa2b2pm,0,-Pa2b2mm;
   1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0;
   0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0;
   0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0;
   0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1];
%}

M=[1,1,0,0,0,0,0,0,-1,-1,0,0,0,0,0,0; ...,
    0,0,1,1,0,0,0,0,0,0,-1,-1,0,0,0,0; ...,
    0,0,0,0,1,1,0,0,0,0,0,0,-1,-1,0,0; ...,
    0,0,0,0,0,0,1,1,0,0,0,0,0,0,-1,-1; ...,
    1,0,1,0,-1,0,-1,0,0,0,0,0,0,0,0,0; ...,
    0,1,0,1,0,-1,0,-1,0,0,0,0,0,0,0,0; ...,
    0,0,0,0,0,0,0,0,1,0,1,0,-1,0,-1,0; ...,
    0,0,0,0,0,0,0,0,0,1,0,1,0,-1,0,-1; ...,
    Pa1b1pm,-Pa1b1pp,0,0,0,0,0,0,0,0,0,0,0,0,0,0; ...,
    0,0,Pa1b1mm,-Pa1b1mp,0,0,0,0,0,0,0,0,0,0,0,0; ...,
    0,0,0,0,Pa2b1pm,-Pa2b1pp,0,0,0,0,0,0,0,0,0,0; ...,
    0,0,0,0,0,0,Pa2b1mm,-Pa2b1mp,0,0,0,0,0,0,0,0; ...,
    0,0,0,0,0,0,0,0,Pa1b2pp,0,-Pa1b2mp,0,0,0,0,0; ...,
    0,0,0,0,0,0,0,0,0,Pa1b2pm,0,-Pa1b2mm,0,0,0,0; ...,
    0,0,0,0,0,0,0,0,0,0,0,0,Pa2b2pp,0,-Pa2b2mp,0; ...,
    0,0,0,0,0,0,0,0,0,0,0,0,0,Pa2b2pm,0,-Pa2b2mm; ...,
    1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0; ...,
    0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0; ...,
    0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0; ...,
    0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1];


B=[0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1];
X=linsolve(M,B);

NPa1b1pp=X(1);
NPa1b1pm=X(2);
NPa1b1mp=X(3);
NPa1b1mm=X(4);
NPa2b1pp=X(5);
NPa2b1pm=X(6);
NPa2b1mp=X(7);
NPa2b1mm=X(8);
NPa1b2pp=X(9);
NPa1b2pm=X(10);
NPa1b2mp=X(11);
NPa1b2mm=X(12);
NPa2b2pp=X(13);
NPa2b2pm=X(14);
NPa2b2mp=X(15);
NPa2b2mm=X(16);


if NPa1b1pp>=0 && NPa1b1pp<=1 && NPa1b1mp>=0 && NPa1b1mp<=1 && NPa1b1pm>=0 && NPa1b1pm<=1 && NPa1b1mm>=0 && NPa1b1mm<=1 ...,
        && NPa1b2pp>=0 && NPa1b2pp<=1 && NPa1b2mp>=0 && NPa1b2mp<=1 && NPa1b2pm>=0 && NPa1b2pm<=1 && NPa1b2mm>=0 && NPa1b2mm<=1 ...,
        && NPa2b1pp>=0 && NPa2b1pp<=1 && NPa2b1mp>=0 && NPa2b1mp<=1 && NPa2b1pm>=0 && NPa2b1pm<=1 && NPa2b1mm>=0 && NPa2b1mm<=1 ...,
        && NPa2b2pp>=0 && NPa2b2pp<=1 && NPa2b2mp>=0 && NPa2b2mp<=1 && NPa2b2pm>=0 && NPa2b2pm<=1 && NPa2b2mm>=0 && NPa2b2mm<=1
    
    EA1B1=NPa1b1pp+NPa1b1mm-NPa1b1mp-NPa1b1pm;
    EA1B2=NPa1b2pp+NPa1b2mm-NPa1b2mp-NPa1b2pm;
    EA2B1=NPa2b1pp+NPa2b1mm-NPa2b1mp-NPa2b1pm;
    EA2B2=NPa2b2pp+NPa2b2mm-NPa2b2mp-NPa2b2pm;
    
    
    f=abs(EA1B1-m*EA2B1-m*n*EA2B2-n*EA1B2);
else
    f=0;
end

value = -f;
end

