function [e, tau3, R2] = target(x, Np, Nc, targetmat, kappa)
% --------------------------------------------------------------
% res = target_new(x, Np, Nc, targetmat, kappa)
%
% Apply contraints to the set of x
%
% Parameters
%   xin     = input x, before applying constraints
%
% Returns
%   xout    = output x, after applying constraints
% --------------------------------------------------------------
% apply constraint to x vector
[q, c] = xconstraint(x, Np, Nc);

mat = zeros(Nc, Nc);
for i = 1:Np
    mat = mat + q(i)*q(i) * c(i, :)' * c(i, :);
end

errmat = mat - targetmat;
errvec = reshape(errmat, [numel(errmat), 1]);
R2 = sum(errvec' * errvec);

tau3 = threeTangle(q, c, Np, Nc);

e = tau3 + kappa * R2;