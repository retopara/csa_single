function [xout] = xconstraint(xin)
% --------------------------------------------------------------
% [xout] = constraint(xin)
%
% Apply contraints to the set of x
%
% Parameters
%   xin     = input x, before applying constraints
%
% Returns
%   xout    = output x, after applying constraints
% --------------------------------------------------------------

N = length(xin);
xout = zeros(N, 1);

upbound1 = 1;
lowbound1 = 0;
range1 = upbound1 - lowbound1;

for i = 1:N
    if (xin(i) > upbound1) xout(i) = xin(i) - range1;
    elseif (xin(i) < lowbound1) xout(i) = xin(i) + range1;
    else xout(i) = xin(i);
    end
end
