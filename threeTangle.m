function tau3 = threeTangle(q, c, Np, Nc)
% --------------------------------------------------------------
% res = target_new(x, Np, Nc, targetmat, kappa)
%
% Apply contraints to the set of x
%
% Parameters
%   xin     = input x, before applying constraints
%
% Returns
%   xout    = output x, after applying constraints
% --------------------------------------------------------------
if Nc ~= 8
    error(' ERROR[threeTangle]: cannot calculate three tangle when Nc~=8');
end

tau3 = 0;
for i = 1:Np
    phi000 = c(i, 1);
    phi001 = c(i, 2);
    phi010 = c(i, 3);
    phi011 = c(i, 4);
    phi100 = c(i, 5);
    phi101 = c(i, 6);
    phi110 = c(i, 7);
    phi111 = c(i, 8);
    
    d1 = phi000^2 * phi111^2 ...
        + phi001^2 * phi110^2 ...
        + phi010^2 * phi101^2 ...
        + phi100^2 * phi011^2;
    
    d2 = phi000*phi111*phi011*phi100 ...
        + phi000*phi111*phi101*phi010 ...
        + phi000*phi111*phi110*phi001 ...
        + phi011*phi100*phi101*phi010 ...
        + phi011*phi100*phi110*phi001 ...
        + phi101*phi010*phi110*phi001;
    
    d3 = phi000*phi110*phi101*phi011 ...
        + phi111*phi001*phi010*phi100;

    tau3 = tau3 + q(i)* q(i) * 4 * abs(d1 - 2*d2 + 4* d3);
end
    
    