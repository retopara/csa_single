function disppara(seed, N, alpha0, e0, deltaE0, Nadjust, Ncool, csa_max, tol, betamin, betamax, coolrate, OUT_MC)
% --------------------------------------------------------------
% disppara()
%
% Display the parameters of constrained simulated annealing
%
% Parameters
%   seed    = The random number seed
%   N   = The number of parameters, i.e., x in f(x)
%   alpha0  = The initial value of step length coeff alpha.
%   e0      = The initial value of target function
%   deltaE0 = The initial value of enew - eold
%   Nadjust = The num of steps before a step adjustment
%   Ncool   = The num of steps before a cooling of temperature
%   csa_max = The max num of csa steps
%   tol     = The tolerance of error of the final result
%   betamin, betamax = The min and max of beta value
%   coolrate= The multiplier of beta in cooling scheme
%   OUT_MC  = 1- output the details of monte carlo steps, 0- don't
%
% Returns
%   Display of parameters
% --------------------------------------------------------------

fprintf('\n\n -----> The Parameters are');
fprintf('\n seed \t\t= %d', seed);
fprintf('\n N \t\t\t= %d', N);
fprintf('\n e0 \t\t= %.1f', e0);
fprintf('\n alpha0 \t= %.1f', alpha0);
fprintf('\n deltaE0 \t= %.1f', deltaE0);
fprintf('\n Nadjust \t= %d', Nadjust);
fprintf('\n Ncool \t\t= %d', Ncool);
fprintf('\n csa_max \t= %.1e', csa_max);
fprintf('\n tol \t\t= %.1e', tol);
fprintf('\n betamin \t= %.1f', betamin);
fprintf('\n betamax \t= %.1e', betamax);
fprintf('\n coolrate \t= %.1f', coolrate);
fprintf('\n OUT_MC \t= %d', OUT_MC);
