function x = convert2x(q, c, Np, Nc)
% --------------------------------------------------------------
% x = convert2x(q, c, Np, Nc)
%
% this function convert coefficient q(Np) and c(Np, Nc) back to x(Np+ 2*Np*Nc)
% 
% --------------------------------------------------------------

x(1:Np) = q;
j = Np;
for i = 1:Np
    x(j+1:j+Nc, :) = real(c(i, :));
    x(j+Nc+1:j+2*Nc, :) = imag(c(i, :))/1i;
end