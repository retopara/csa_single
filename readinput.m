function [ri, Nsweep, str] = readinput()
% ---------------------------------------------------------------
% readinput()
%
% This function read input parameters written in the following format:
%
% name1 = value1
% % comment line 1
% name2 = value2
% % comment line 2
% name3 = value3
% ......
% EOF
%
% and stores the input parameters in the struct: Parameter
% ---------------------------------------------------------------
fid = fopen('input', 'r');

str = '';
while 1
    oneline = fgetl(fid);
    if strcmp(oneline, 'matr')
        ri.matr = textscan(fid,'%f %f %f %f %f %f %f %f', 'Delimiter',' ' ,'MultipleDelimsAsOne',1, 'CollectOutput',1);
        ri.matr = ri.matr{1};
        continue;
    elseif strcmp(oneline, 'mati')
        ri.mati = textscan(fid,'%f %f %f %f %f %f %f %f', 'Delimiter',' ' ,'MultipleDelimsAsOne',1, 'CollectOutput',1);
        ri.mati = ri.mati{1};
        continue;
    elseif ~isempty(strfind(oneline, '%'))
        % check if there is an '%' inside a string, which makes it a comment instead of an input line
        continue;
    elseif ~ischar(oneline) || isempty(oneline)
        % break if oneline is empty or EOF
        break;
    end
    eval(['ri.', oneline, ';']);
    str = [str, char(10), ' ', oneline]; % char(10) is \n
end
fclose(fid);

Nsweep = ri.Nsweep;
ri.Nc = ri.Nlev^ ri.Npar;
ri.N = ri.Np + 2* ri.Np * ri.Nc;
ri.targetmat = ri.matr + 1i* ri.mati;

str = [str, sprintf('\n Nc = %d', ri.Nc)];
str = [str, sprintf('\n N = %d', ri.N)];
str = [str, sprintf('\n matr = ')];
for i = 1:size(ri.matr, 1)
    str = [str, sprintf('\n')];
    str = [str, sprintf('\t %f', ri.matr(i, :))];
end
str = [str, sprintf('\n mati = \n')];
for i = 1:size(ri.mati, 1)
    str = [str, sprintf('\n')];
    str = [str, sprintf('\t %f', ri.mati(i, :))];
end