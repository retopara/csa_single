 function main()
% --------------------------------------------------------------
% main()
%
% This the main function of csa program
%
% Parameters
%   A
%
% Returns
%   B
% --------------------------------------------------------------

% clear the screen and memory
clc;
clear;

fprintf('\n\n Cooling CSA Program Start ^_^');

% read input
[ri, Nsweep, str] = readinput();

if ri.OUT_MC == 1
    % display parameters
    fprintf('\n\n -----> The Parameters are: ');
    fprintf('\n %s', str);
end

% single thread version
rng('shuffle');
ri.seed = randi(1E7, 1);
[ro] = csa(ri.seed, ri.N, ri.Np, ri.Nc, ri.Npar, ri.Nlev, ri.kappa, ri.targetmat, ...
ri.Nadjust, ri.csamax, ri.tol, ri.e0, ri.alpha0, ...
ri.betamin, ri.betamax, ri.coolrate, ri.Ncool, ri.OUT_MC);