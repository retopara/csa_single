function [alphaout] = adjuststep(alphain, acpt_rate)
% --------------------------------------------------------------
% [alphaout] = adjuststep(alphain, acpt_rate, N)
%
% Adjust the step length of x according to the acceptance rate of
% metropolis steps.
% this function adjust step length according to acpt_rate.
% if acpt_rate is high, then take a larger step, else if acpt_rate
% is low, then take a smaller step. always keep acpt_rate between
% 0.2 and 0.3, which is a optimal range.
%
% Parameters
%   alphain     = The original step length
%   acpt_rate   = The acceptance rate of metropolis
%   N           = The number of x
%
% Returns
%   alphaout    = The adjusted step length
% --------------------------------------------------------------

N = length(alphain);
alphaout = zeros(N, 1);

acpt_max = 0.3;
acpt_min = 0.2;
sigma = 2;

for i = 1:N
    if(acpt_rate(i) > acpt_max)
        alphaout(i) = alphain(i) * (1 + sigma * (acpt_rate(i) - acpt_max)/(1 - acpt_max));
    elseif(acpt_rate(i) < acpt_min)
        alphaout(i) = alphain(i) / (1 + sigma * (acpt_min - acpt_rate(i))/(acpt_min - 0));
    else
        alphaout(i) = alphain(i);
    end
end