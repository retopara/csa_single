function [xout] = testxconstraint(xin)
% --------------------------------------------------------------
% [xout] = constraint(xin)
%
% Apply contraints to the set of x
%
% Parameters
%   xin     = input x, before applying constraints
%
% Returns
%   xout    = output x, after applying constraints
% --------------------------------------------------------------

N = length(xin);
xout = zeros(N, 1);

upbound1 = 20;
lowbound1 = -20;

for i = 1:2
    if (xin(i) > upbound1) xout(i) = xin(i) - 40;
    elseif (xin(i) < lowbound1) xout(i) = xin(i) + 40;
    else xout(i) = xin(i);
    end
end