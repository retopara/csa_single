function [q, c] = xconstraint(x, Np, Nc)
% --------------------------------------------------------------
% [xout] = constraint(x)
%
% Apply contraints to the set of x
%
% Parameters
%   xin     = input x, before applying constraints
%
% Returns
%   xout    = output x, after applying constraints
% --------------------------------------------------------------

[q, c] = xconvert(x, Np, Nc);
norm = sqrt(sum(q.^2));
q = q / norm;

for i = 1:Np
    norm = sqrt(c(i, :) * c(i, :)');
    c(i, :) = c(i, :) / norm;
end
