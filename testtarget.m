function [value] = testtarget(x)
% --------------------------------------------------------------
% [value] = target(A1,A2,B1,B2,va1,va2,vb1,vb2,a1,a2,a3,a4)
%
% The target function
%
% Parameters
%   A1,A2,B1,B2,va1,va2,vb1,vb2,a1,a2,a3,a4     = The array of x
%
% Returns
%   value    = The value of F(x)
% --------------------------------------------------------------

A = -10;
U = 1;
value = A * (x(1) - 2* x(1)^2 + 2* x(1)* sqrt(x(2) * (1-2*(x(2)-2*x(1))))) + 2*x(1)* U;
