function [q, c] = xconvert(x, Np, Nc)
% --------------------------------------------------------------
% [q, c] = xconvert(x, Np, Nc)
% 
% this function convert variable vector x(Np+ 2*Np*Nc) to 
% coefficient q(Np) and c(Np, Nc)
% 
% --------------------------------------------------------------

q = x(1:Np);

c = NaN * ones(Np, Nc);
j = Np;
for i = 1:Np
    c(i, :) = x(j+1 : j+Nc) + 1i*x(j+Nc+1 : j+2*Nc);
    j = j + 2*Nc;
end