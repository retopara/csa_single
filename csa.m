function [ro] = csa(seed, N, Np, Nc, Npar, Nlev, kappa, targetmat, ...
    Nadjust, csamax, tol, e0, alpha0, ...
    betamin, betamax, coolrate, Ncool, OUT_MC)
% --------------------------------------------------------------
% [ro] = csa(seed, N, Nadjust, csa_max, tol, e0, deltaE0, alpha0, betamin, betamax, coolrate, Ncool, OUT_MC)
%
% This is a optimization program using simulated annealing
%
% Parameters
%   seed    = The random number seed
%   N   = The number of parameters, i.e., x in f(x)
%   alpha0  = The initial value of step length coeff alpha.
%   e0      = The initial value of target function
%   deltaE0 = The initial value of enew - eold
%   Nadjust = The num of steps before a step adjustment
%   Ncool   = The num of steps before a cooling of temperature
%   csa_max = The max num of csa steps
%   tol     = The tolerance of error of the final result
%   betamin, betamax = The min and max of beta value
%   coolrate= The multiplier of beta in cooling scheme
%   OUT_MC  = 1- output the details of monte carlo steps, 0- don't
%
% Returns
%   ro  = The output struct
% --------------------------------------------------------------

tic
rng(seed);

% initializations
x = 2*rand(N, 1) - 1;
xnew = x;
xmin = x;
xstep = zeros(N, 1);

alpha = alpha0 * ones(N, 1); % the original step length coeff alpha
acpt_count = zeros(N, 1);
acpt_rate = zeros(N, 1);

emin = e0;
tau3min = NaN;
R2min = NaN;
e = e0;
deltaE = e0;
beta = betamin;
csa_count = 0;

while(deltaE > tol && csa_count < csamax)
    acpt_flag = 0;
    csa_count = csa_count + 1;
    for ix = 1:N
        xstep(ix) = alpha(ix)*(2*rand - 1);
        xnew(ix) = x(ix) + xstep(ix);
        [enew, tau3, R2] = target(xnew, Np, Nc, targetmat, kappa);
%         enew = testtarget(xnew);
%         enew = target(xnew(1), xnew(2), xnew(3), xnew(4), ...,
%             xnew(5), xnew(6), xnew(7), xnew(8), ...,
%             xnew(9), xnew(10), xnew(11), xnew(12), ...,
%             xnew(13), xnew(14), xnew(15), xnew(16), ...,
%             xnew(17), xnew(18), xnew(19), xnew(20));
        
        % metropolis algorithm
        if(imag(enew) == 0) 
            if(enew < e)
                x = xnew;
                e = enew;
                acpt_flag = 1;
                acpt_count(ix) = acpt_count(ix) + 1;
                if(enew < emin)
                    deltaE = emin - enew;
                    xmin = xnew;
                    emin = enew;
                    tau3min = tau3;
                    R2min = R2;
                end
            else
                r = rand;
                p = exp(-beta * (enew - e));
                if(r < p)
                    x = xnew;
                    e = enew;
                    acpt_flag = 1;
                    acpt_count(ix) = acpt_count(ix) + 1;
                else
                    xnew = x;
                    enew = e;
                    acpt_flag = 0;
                end
            end
        end
    end
    
    % adjust xstep according to acpt rate
    if(mod(csa_count, Nadjust) == 0)
        if OUT_MC == 1
            fprintf('\n\n -----> Adjusting step length... csa_count = %d\n', csa_count);
            fprintf('\n deltaE = %.1e \temin = %f\t tau3min = %f R2min = %f', deltaE, emin, tau3min, R2min);
            if OUT_MC == 2
                fprintf('\n xmin are:');
                for i = 1:N
                    fprintf('\n i = %d, xmin = %.12f', i, xmin(i));
                end
            end
        end
        
        for i = 1:N
            acpt_rate(i) = acpt_count(i) * 1.0 / Nadjust;
        end
        if OUT_MC == 2
            fprintf('\n\n alphas(x step length coeffs) are:');
            for i = 1:N
                fprintf('\n i = %d, alpha before adjust = %f\t acpt_rate = %f \txstep_old = %f', i, alpha(i), acpt_rate(i), xstep(i));
            end
        end
        alpha = adjuststep(alpha, acpt_rate);
        if OUT_MC == 2
            for i = 1:N
                fprintf('\n i = %d, alpha after adjust = %f\t', i, alpha(i));
            end
        end
        acpt_count = zeros(N, 1);
    end
    
    if mod(csa_count, Ncool) == 0
        if OUT_MC == 1
            fprintf('\n\n -----> Cooling... csa_count = %d\n', csa_count);
            fprintf('\n beta before cooling = %.1e, \tcoolrate = %.2f', beta, coolrate);
        end
        beta = beta * coolrate;
        if beta > betamax 
            beta = betamax; 
        end
        if OUT_MC == 1
            fprintf('\n beta after cooling = %.1e', beta);
        end
    end
    
    if(deltaE < tol)
        fprintf('\n\n -----> Converge! csa_count = %d, Results are:\n', csa_count);
        fprintf('\n deltaE = %.1e, \ttol = %.1e, \tbeta = %.1e', deltaE, tol, beta);
        fprintf('\n emin = %.16f\tor %e', emin, emin);
        fprintf('\n max value = %.16f\tor %e', -emin, -emin);
        fprintf('\n xmin are:');
        for i = 1:N
            fprintf('\n i = %d, xmin = %.12f', i, xmin(i));
        end
        ro.xmin = xmin;
        ro.emin = emin;
        ro.cputime = toc;
        fprintf('\n\n -----> The cpu time(seconds) = %f\n', cputime);
        fprintf('\n Program End ^_^\n');
        
    end
end
